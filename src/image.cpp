#include "image.hpp"
#include <limits>
#include <stdlib.h>


using namespace std;

Image::Image(){
  caminhoDaImagem;
  numeroMagico;
  altura = 0;
  largura = 0;
  cor = 0;
}
void Image::setCaminhoDaImagem(){

  cout << "Digite o caminho da imagem: " << endl;
    cin >> caminhoDaImagem;

}
 void Image::setCabecalho(ifstream &file){
  char c;
    file >> numeroMagico;
    while(file.get(c)){
      if(c == '#'){
        file.ignore(numeric_limits<char>::max(),'\n');
        break;
      }
    }

   file >> largura;
   file >> altura;
   file >> cor;

 }

 string Image::getNumeroMagico(){
    return numeroMagico;
 }

 char *Image::getCaminhoDaImagem(){
     return caminhoDaImagem;

 }

 int Image::getAltura(){
   return altura;
 }

 int Image::getLargura(){
   return largura;
 }

 int Image::getCor(){
   return cor;
 }
