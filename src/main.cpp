#include "image.hpp"
#include "pgm.hpp"
#include "ppm.hpp"


using namespace std;

int main(){
  Pgm *imagemPgm = new Pgm();
  Ppm *imagemPpm = new Ppm();

  char *opcao = new char;
  cout << "Digite o número correspondente a operação desejada: "<<endl;
  cout << "1.Ler mensagem da Imagem Pgm" << endl;
  cout << "2.Aplicar o filtro a Imagem Ppm" << endl;
  cout << "0.Sair" << endl;
  cout << "Opção: ";cin.get(opcao,256,'\n');

  switch (*opcao) {
    case '1':imagemPgm->executarPgm(imagemPgm);break;
    case '2':imagemPpm->executarPpm(imagemPpm);break;
    case '0':return 0;break;

    default : cout << "Opção inválida"<<endl;

  }

    return 0;
}
