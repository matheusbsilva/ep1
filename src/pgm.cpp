#include "pgm.hpp"
#include <cstdlib>

using namespace std;

Pgm::Pgm(){
  caminhoDaImagem;
  byteFile=0;
  bitExtraido=0;
  caracter = 0;
}

void Pgm::setPosicaoInicial(){
  cout << "Digite a posição inicial da mensagem contida na imagem pgm: ";
  cin >> posicaoInicial;
  posicaoInicial = (posicaoInicial + 1);//+1 por conta da quebra de linha
}

void Pgm::decifrar(ifstream &file){
  file.seekg(file.tellg()+posicaoInicial,file.beg);

  while(caracter != '#'){
    for (int i = 0; i < 8; i++) {
      file.get(byteFile);
      bitExtraido = (byteFile & 0x01);
      caracter = (caracter << 1) | (bitExtraido & 0x01);
    }
    cout << caracter;
  }
}

void Pgm::executarPgm(Pgm *pgm){
  pgm->setCaminhoDaImagem();
  ifstream file (pgm->getCaminhoDaImagem(),ifstream :: in | ifstream::binary );
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
  }
  pgm->setPosicaoInicial();
  pgm->setCabecalho(file);
  pgm->decifrar(file);
  cout << endl;

  file.close();
  delete(pgm);
}
