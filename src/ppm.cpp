#include "ppm.hpp"
#include <cstdlib>
#include <fstream>


using namespace std;

Ppm::Ppm(){
}
void Ppm::lerPpm(ifstream &file,Ppm *ppm){
  ppm->pixel = new Ppm::Rgb[ppm->getLargura() * ppm->getAltura()];
  char pix[3];
  for(int i=0;i< ppm->getLargura() * ppm->getAltura();i++){
    file.read(reinterpret_cast<char *>(pix), 3);
    ppm->pixel[i].r = pix[0];
    ppm->pixel[i].g = pix[1];
    ppm->pixel[i].b = pix[2];
  }
}

void Ppm::escreverPpm(Ppm *ppm){
  ofstream file("img/resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = ppm->pixel[i].r;
    g = ppm->pixel[i].g;
    b = ppm->pixel[i].b;
    file << r << g << b;
  }
  file.close();
}

void Ppm::filtroAzul(Ppm *ppm){
  ofstream file("img/resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = 0;
    g = 0;
    b = 255-ppm->pixel[i].b;
    file << r << g << b;
  }
  file.close();
}

void Ppm::filtroVerde(Ppm *ppm){
  ofstream file("img/resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = 0;
    g = 255-ppm->pixel[i].g;
    b = 0;
    file << r << g << b;
  }
  file.close();
}

void Ppm::filtroVermelho(Ppm *ppm){
  ofstream file("img/resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r =255-ppm->pixel[i].r;
    g = 0;
    b = 0;
    file << r << g << b;
  }
  file.close();

}

void Ppm::escolhaFiltro(Ppm *ppm){
  int opcao;
  cout << "Selecione o filtro: " << endl;
  cout << "1.Filtro vermelho"<<endl;
  cout << "2.Filtro verde"<<endl;
  cout << "3.Filtro azul" <<endl;
  cout << "Digite: "; cin >> opcao;

  switch (opcao) {
    case 1: return ppm->filtroVermelho(ppm);
            break;
    case 2: return ppm->filtroVerde(ppm);
            break;
    case 3: return ppm->filtroAzul(ppm);
            break;
  }
}

void Ppm::executarPpm(Ppm *ppm){
  ppm->setCaminhoDaImagem();
  ifstream file (ppm->getCaminhoDaImagem(),ifstream :: in | ifstream::binary );
   if(!file){
     cerr << "Não foi possível abrir o arquivo" << endl;
     exit(1);
   }
   ppm->setCabecalho(file);
   ppm->lerPpm(file,ppm);
   ppm->escolhaFiltro(ppm);

   file.close();
   delete(ppm);

}
