#ifndef IMAGE_H
#define IMAGE_H

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Image{
  //Atributos
  protected:
    char caminhoDaImagem[256];
    string numeroMagico;
    int altura;
    int largura;
    int cor;
  //Métodos
  public:
    Image();

    void setCaminhoDaImagem();
    void setCabecalho(ifstream &file);
    void setCabecalhoP(ifstream &file);

    char *getCaminhoDaImagem();
    int getPosicaoInicial();
    string getNumeroMagico();
    int getAltura();
    int getLargura();
    int getCor();

};

#endif
