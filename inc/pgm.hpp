#ifndef PGM_H
#define PGM_H

#include "image.hpp"

using namespace std;

class Pgm : public Image {
  private:
    int posicaoInicial;
    char byteFile;
    char bitExtraido;
    char caracter;

  public:
    Pgm();

    void decifrar(ifstream &file);
    void setPosicaoInicial();
    void executarPgm(Pgm *pgm);
};

#endif
