#ifndef PPM_H
#define PPM_H

#include "image.hpp"

using namespace std;
class Ppm : public Image {

  public:
    //Atributos
    struct Rgb{
      char r,g,b;
      Rgb(): r(0), g(0), b(0) {}
      Rgb(char _r,char _g, char _b) : r(_r), g(_g), b(_b){}
    };
    Rgb *pixel;



    Ppm();
    void lerPpm(ifstream &file,Ppm *ppm);
    void escreverPpm(Ppm *ppm);
    void escolhaFiltro(Ppm *ppm);
    void filtroAzul(Ppm *ppm);
    void filtroVerde(Ppm *ppm);
    void filtroVermelho(Ppm *ppm);
    void executarPpm(Ppm *ppm);


};
#endif
