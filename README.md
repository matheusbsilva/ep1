Orientação a Objetos 1/2016--Matheus Batista Silva 15/0018029
==============================================================
Exercício de Programação 01
--------------------------------------------------------------

Intruções para execução do programa:

  01-Abra o terminal do sistema Unix;

  02-Localize a pasta do programa usando o terminal;

  03-Digite makefile no terminal;

  04-Então digite make run para iniciar o programa;

  05-Selecione a operação desejada;

  06-Caso selecione a "Ler a imagem Pgm", será pedido o caminho da imagem e a mensagem será escrita na tela;

  07-Caso selecione "Aplicar filtro na imagem ppm", será pedido o caminho da imagem e o filtro que deseja aplicar, e será gerada uma imagem "resultado" com o filtro aplicado no diretório "img" do programa.

Observações:

  -Ao digitar o caminho da imagem que deseja ler, pode simplesmente digitar "img/nome da imagem desejava", pois todas as imagens disponibilizadas estão presente no diretório "img" do programa,

  -Existe um detalhe com imagem "lena.pgm", como a mensagem presente na imagem inicia com "#" que no programa é considerado um valor de quebra no laço da leitura, a posição inicial deve ser passada de forma que ignore esse primeiro caractere, como padrão a mensagem na imagem lena começa na posição 50000, ignorando o caractere deve-se passar a posição 50008;
